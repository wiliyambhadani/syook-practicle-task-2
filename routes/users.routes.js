
const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth")
const controller = require("../controller/user")

router.post("/auth", controller.post)

module.exports = router;

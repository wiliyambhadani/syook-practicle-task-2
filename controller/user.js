
const Joi = require('joi');
const Users = require("../models/Users")
const logger = require("../config/logger")


const Controller = {}

///login routes
Controller.post = async (req, res) => {
    try {
        console.log("req.body-->>", req.body)
        let bodySchema = Joi.object({
            email: Joi.string().required(),
            password: Joi.string().required()
        })
        const validate = bodySchema.validate(req.body);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }
        let resData = await Users.findOne({ email: req.body.email, password: req.body.password })
        if (!resData) {
            res.send({
                msg: "User not found",
                data: []
            }).status(404)
        }
        // res.send({
        //     msg: "OK",
        //     data: {
        //         token: "b82b414b-7ec7-4c4d-8df8-adeb32ec6ef0"
        //     }
        // }).status(200)
        res.redirect('/ui?token=' + "b82b414b-7ec7-4c4d-8df8-adeb32ec6ef0");
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}


module.exports = Controller
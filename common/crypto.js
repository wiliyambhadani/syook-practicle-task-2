const crypto = require('crypto')
const algorithm = 'aes-256-ctr'
const password = 'd6F3Efeq';



const encrypt = (text) => {
    const cipher = crypto.createCipher(algorithm, password)
    let crypted = cipher.update(text, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

const decrypt = (text) => {
    const decipher = crypto.createDecipher(algorithm, password)
    let dec = decipher.update(text, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}


const encrypt256 = (text) => {
    const hash = crypto.createHash('sha256').update(text).digest('hex');
    return hash;
}




module.exports = { encrypt, decrypt, encrypt256 }
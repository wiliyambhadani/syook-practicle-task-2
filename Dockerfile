# Dockerfile

FROM node:12.13.0-alpine

WORKDIR /app

COPY . .

RUN npm i -g npm

RUN npm install --no-optional

USER root

# Command to run the executable
ENTRYPOINT ["node", "server.js"]
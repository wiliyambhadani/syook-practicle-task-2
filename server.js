require('dotenv').config()
const express = require('express')
const path = require("path")
const cors = require('cors');
const morgan = require('morgan')
const bodyParser = require('body-parser');
const db = require("./library/mongoose")
const logger = require("./config/logger")
const routes = require("./routes")
const app = express()
const Users = require("./models/Users")
const Person = require("./models/Person")
const auth = require("./middleware/auth")
const njsListner = require("./njs-listner")



const addUser = async () => {
    let data = {
        email: "abc@gmail.com",
        password: "abc@123"
    }

    let isExist = await Users.findOne({ email: data.email })
    if (isExist) {
        console.log("user is exist")
        return true
    }
    console.log("user not exist creating one")
    let user = new Users(data)
    await user.save()
    return true

}

const startServer = async () => {
    await db.conectDb()
    await addUser()
    app.use(cors());
    app.use(express.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.options('*', cors());
    app.set('etag', false)
    //setup public folder
    app.use(express.static('./public'));
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
    app.get("/ping", (req, res) => {
        res.send({
            msg: "Ok Pong!",
            data: []
        }).status(200)
    })
    app.get("/login", (req, res) => {
        res.sendFile(__dirname + '/public/login.html');

    })

    app.get("/ui", auth.checkToken, (req, res) => {
        res.sendFile(__dirname + '/public/index.html');

    })

    app.get("/data", async (req, res) => {
        let data = await Person.find({}, "name origin destination timeStamp").sort("-timeStamp").limit(100)
        res.send({
            msg: "Ok",
            data: data
        }).status(200)
    })

    app.use(routes)
    //Listen on port 3000
    server = app.listen(process.env.PORT)

    logger.info(`Server is up at localhost: ${process.env.PORT}`)

    await njsListner.startNJSListner()
}

startServer()
const mongoose = require('mongoose')

const PersonSchema = mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    origin: {
        type: String,
        required: true
    },
    destination: {
        type: String,
        required: true
    },
    timeStamp: {
        type: Date,
        require: true
    }

}, {
    timestamps: true
})



module.exports = mongoose.model("Person", PersonSchema)
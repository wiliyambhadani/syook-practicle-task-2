//server.js
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const alg = require("./common/crypto");
const Person = require("./models/Person")



const startNJSListner = () => {
    io.on('connection', function (socket) {
        console.log('connection');

        socket.on('data', async function (from, msg) {
            console.log("got data--->>>>")
            msg = msg.split("|")
            //decrypt data 
            let resData = []
            for (let i = 0; i < msg.length; i++) {
                if (msg[i] != "") {
                    let decryptData = alg.decrypt(msg[i])
                    decryptData = JSON.parse(decryptData)
                    let hash = alg.encrypt256(decryptData.name + decryptData.origin + decryptData.destination)
                    if (hash === decryptData.passkey) {
                        resData.push(decryptData)
                        decryptData.timeStamp = new Date()
                        let person = new Person(decryptData)
                        await person.save()
                    }
                }
            }
        });

    });

    http.listen(3000, function () {
        console.log('socket server listening on *:3000');
    });
}

module.exports = { startNJSListner }
//client.js
const io = require('socket.io-client');
const socket = io.connect('http://127.0.0.1:3000', { reconnect: true });

const alg = require("./crypto")


// Add a connect listener
socket.on('connect', function (socket) {
    console.log('Connected!');
});

setInterval(async () => {

    socket.emit('data', 'device1', await generateData());

}, 10000)//publish at every 1 sec

const cities = [
    "delhi", "mumbai", "goa", "bengaluru", "chennai", "surat", "navsari", "pune", "almora", "anjar"
]
const generateData = async () => {
    let data = {}
    let dataString = ""
    for (let i = 0; i < 10; i++) {
        data = {
            name: "user " + i,
            origin: cities[Math.floor(Math.random() * cities.length)],
            destination: cities[Math.floor(Math.random() * cities.length)],
        }
        data.passkey = alg.encrypt256(data.name + data.origin + data.destination)
        data = alg.encrypt(JSON.stringify(data))
        dataString += data + "|"
    }
    return dataString
}
